﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Threading;
using ClientLib;
using ClientServerLib;
using System.Windows.Threading;
using ToRinexConverter;

namespace TestClientWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        string text = "";

        string host = "0";
        int port = 0;
        private Client _clientGnss;

        private Thread almThread;
        private Thread ephThread;
        private Thread satThread;
        private Thread recThread;

        private string _almTime = "";
        private string _ephTime = "";
        private string _satTime = "";
        private string _recTime = "";

        private readonly AlmanacConverter almanacConverter;
        private readonly EphemerisConverter ephemerisConverter;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            almanacConverter = new AlmanacConverter();
            ephemerisConverter = new EphemerisConverter();
        }

        public string TextMess
        {
            get { return text; }
            set
            {
                text = value;
                PropertyChanged(this, new PropertyChangedEventArgs("TextMess"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        private void BtnConnect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                

                if (_clientGnss != null)
                {
                    _clientGnss = null;
                }
                btnConnect.IsEnabled = false;
                host = tbxIp.Text;
                port = Convert.ToInt16(tbxPort.Text);
                if (!host.Equals("0") && port != 0)
                {
                    _clientGnss = new Client(host, port);
                    if (_clientGnss.CheckHandler())
                    {
                        _clientGnss.OnGetData += ProcessIncomeData;
                        _clientGnss.OnDisconnected += clientGnss_OnDisconnected;
                        _clientGnss.OnConnected += _clientGnss_OnConnected;
                        _clientGnss.OnError += clientGnss_OnError;
                    }
                    _clientGnss.Connect();
                    
                }
                else
                {
                    TextMess += "Рost or port data are incorrect!"+ "\n";
                }
                btnDisonnect.IsEnabled = true;
            }
            catch (Exception ex)
            {
                TextMess += "Port or host values are incorrect!" + "\n";
            }
        }

        private void clientGnss_OnError(object sender, string e)
        {
            TextMess += $"\n Error: " + e + "\n";
        }

        private void _clientGnss_OnConnected(object sender, bool e)
        {
            if (e == true)
            {
                TextMess += $"\n Connected to server" + "\n";
            }
            else
                TextMess += $"\n Can't connect to server" + "\n";
        }

        private void clientGnss_OnDisconnected(object sender, bool e)
        {
            TextMess += $"\n Disconnected from server" + "\n";
        }

        public void ProcessIncomeData(object sender, MyEventArgsGetData e)
        {
            try
            {
                //string objFull = null;

                switch (e.Command)
                {
                    case 1:
                        //foreach (Ephemeries item in _clientGnss.ephemeries)
                        //{
                        //    objFull += "\n Ephemeris (" + item.sv_number.ToString() + ") \n";
                        //    IEnumerable<FieldInfo> fields = item.GetType().GetTypeInfo().DeclaredFields;
                        //    foreach (var field in fields.Where(x => !x.IsStatic)) //вывод всех полей объекта и их значений
                        //    {
                        //        objFull += field.Name + " = " + field.GetValue(item) + "\n";
                        //    }
                        //}
                        //TextMess += "\n Get Ephemeris: \n" + objFull + "\n";

                        TextMess += "\n Got Ephemeris " + _clientGnss.ephemeries.Count;
                        //преобразование в файлы для спуфинга
                        if (_clientGnss.ephemeries.Count == 32)
                        {
                            ephemerisConverter.WriteToRinex(null, _clientGnss.ephemeries, 1);
                        }
                        break;

                    case 2:
                        //foreach (Almanac item in _clientGnss.almanac)
                        //{
                        //    objFull += "\n Almanac (" + item.PRN.ToString() + ") \n";
                        //    IEnumerable<FieldInfo> fields = item.GetType().GetTypeInfo().DeclaredFields;
                        //    foreach (var field in fields.Where(x => !x.IsStatic))
                        //    {
                        //        objFull += field.Name + " = " + field.GetValue(item) + "\n";
                        //    }
                        //}
                        //TextMess += "\n Got Almanacs: \n" + objFull + "\n";

                        TextMess += "\n Got Almanacs " + _clientGnss.almanac.Count;
                        if (_clientGnss.almanac.Count == 32)
                        {
                            almanacConverter.WriteToSEM(null, _clientGnss.almanac, 1);
                        }
                        else
                        {
                            RadarControl.UpdateAlmanacState(3);
                        }
                        break;

                    case 4:
                        //foreach (SatInfo item in _clientGnss.satinfo)
                        //{
                        //    objFull += "\n Sattelite (" + item.PRN.ToString() + ") \n";
                        //    IEnumerable<FieldInfo> fields = item.GetType().GetTypeInfo().DeclaredFields;

                        //    foreach (var field in fields.Where(x => !x.IsStatic))
                        //    {
                        //        objFull += field.Name + " = " + field.GetValue(item) + "\n";
                        //    }
                        //}
                        //TextMess += "\n All Sattelites: \n" + objFull + "\n";
                        TextMess += "\n Got sattelites info " + _clientGnss.satinfo.Count;
                        Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                        {
                            RadarControl.UpdateSatsData(_clientGnss.satinfo);
                        });
                        break;

                    case 5:
                        Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                        {
                            RadarControl.UpdateGNSSCoordinates(_clientGnss.reclocation.ReceiverLatitude, _clientGnss.reclocation.ReceiverLongitude, _clientGnss.reclocation.ReceiverAltitude);
                        });

                        TextMess += "\n Got Receiver location";
                        //IEnumerable<FieldInfo> fieldsLoc = _clientGnss.reclocation.GetType().GetTypeInfo().DeclaredFields;
                        //foreach (var field in fieldsLoc.Where(x => !x.IsStatic))
                        //{
                        //    objFull += field.Name + " = " + field.GetValue(_clientGnss.reclocation) + "\n";
                        //}
                        //TextMess += "\n Receiver location: \n" + objFull + "\n";
                        break;

                    case 6:
                        //TextMess += "\n Time now: \n" +  _clientGnss.time.ToString("dd-MM-yyyy H:mm:s") + "\n";
                        TextMess += "\n Got time" + _clientGnss.time.ToString("dd-MM-yyyy H:mm:s");
                        var newTime = new TimeClass(_clientGnss.time);
                        break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                TextMess += ex.ToString() + "\n"; 
            }
        }

        private void BtnDisonnect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btnConnect.IsEnabled = true;
                _clientGnss.Disconnect();
                _clientGnss = null;
                btnDisonnect.IsEnabled = false;
                TextMess += "Disconnected from server" + "\n";
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    RadarControl.ClearAll();
                });
            }
            catch (Exception ex)
            {
                TextMess += ex.ToString() + "\n";
            }
        }

        private void BtnAlmanac_Click(object sender, RoutedEventArgs e)
        {
            _clientGnss.SendMessage(Operations.Almanac);
        }

        private void BtnEphemeris_Click(object sender, RoutedEventArgs e)
        {
            _clientGnss.SendMessage(Operations.Ephemeries);
        }

        private void BtnSatInfo_Click(object sender, RoutedEventArgs e)
        {
            _clientGnss.SendMessage(Operations.SatInfo);
        }

        private void BtnReceiver_Click(object sender, RoutedEventArgs e)
        {
            _clientGnss.SendMessage(Operations.RecLocation);
        }

        private void BtnTime_Click(object sender, RoutedEventArgs e)
        {
            _clientGnss.SendMessage(Operations.Time);
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            TextMess = "";
        }

        private void BtnAutoStart_Click(object sender, RoutedEventArgs e)
        {
            if (BtnAutoStart.IsChecked == true)
            {
                _almTime = tbxbeforeAlm.Text;
                _ephTime = tbxbeforeEph.Text;
                _satTime = tbxbeforeSat.Text;
                _recTime = tbxbeforeRec.Text; 

                almThread = new Thread(new ThreadStart(AlmTick));
                almThread.IsBackground = true;
                almThread.Start();

                ephThread = new Thread(new ThreadStart(EphTick));
                ephThread.IsBackground = true;
                ephThread.Start();

                satThread = new Thread(new ThreadStart(SatTick));
                satThread.IsBackground = true;
                satThread.Start();

                recThread = new Thread(new ThreadStart(RecTick));
                recThread.IsBackground = true;
                recThread.Start();
            }
            else
            {
                almThread?.Abort();
                ephThread?.Abort();
                satThread?.Abort();
                recThread?.Abort();
                RadarControl.StartTimerAlm(Radar.RadarControl.Timers.Stop, 0);
            }
        }


        #region Tick methods for timers
        private void AlmTick()
        {
            while (_clientGnss != null)
            {
                _clientGnss.SendMessage(Operations.Almanac);
                RadarControl.StartTimerAlm(Radar.RadarControl.Timers.Almanac, Convert.ToDouble(_almTime) * 60);    
                Thread.Sleep(Convert.ToInt32(_almTime) * 60000);
            }
        }


        private void EphTick()
        {
            while (_clientGnss != null)
            {
                _clientGnss.SendMessage(Operations.Ephemeries);
                RadarControl.StartTimerAlm(Radar.RadarControl.Timers.Ephemeris, Convert.ToDouble(_ephTime) * 60);
                Thread.Sleep(Convert.ToInt32(_ephTime) * 60000);
            }
        }


        private void SatTick()
        {
            while (_clientGnss != null)
            {
                _clientGnss.SendMessage(Operations.SatInfo);
                Thread.Sleep(Convert.ToInt32(_satTime) * 1000);
            }
        }


        private void RecTick()
        {
            while (_clientGnss != null)
            {
                _clientGnss.SendMessage(Operations.RecLocation);
                _clientGnss.SendMessage(Operations.Time);
                Thread.Sleep(Convert.ToInt32(_recTime) * 1000);
            }
        }
        #endregion
    }
}
