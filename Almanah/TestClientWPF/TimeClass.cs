﻿using System;
using System.Runtime.InteropServices;

namespace TestClientWPF
{
    public class TimeClass
    {
        [StructLayout(LayoutKind.Sequential)]
        public struct SYSTEMTIME
        {
            public short wYear;
            public short wMonth;
            public short wDayOfWeek;
            public short wDay;
            public short wHour;
            public short wMinute;
            public short wSecond;
            public short wMilliseconds;
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool SetSystemTime(ref SYSTEMTIME st);

        public TimeClass(DateTime GNSStime)
        {
            SYSTEMTIME st = new SYSTEMTIME();
            st.wYear = (short)GNSStime.Year; 
            st.wMonth = (short)GNSStime.Month;
            st.wDay = (short)GNSStime.Day;
            st.wHour = (short)GNSStime.Hour; 
            st.wMinute = (short)GNSStime.Minute;
            st.wSecond = (short)GNSStime.Second;

            SetSystemTime(ref st);
        }
    }
}
