﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;

namespace Radar
{
    class PngVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            byte status = System.Convert.ToByte(value);
            Visibility result = Visibility.Hidden;
            if (status == 3)
            {
                result = Visibility.Visible;
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
