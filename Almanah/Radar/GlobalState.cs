﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System;
using System.Windows.Controls;


namespace Radar
{
    public partial class RadarControl : UserControl, INotifyPropertyChanged
    {
        private DateTime _now;
        private int satelliteCount;
        private double latitude;
        private double longitude;
        private double altitude;
        private string beforeAlmanac;
        private string beforeEphemeris;
        private byte ephState = 0;
        private byte almState = 0; //0-ниче, 1-в процессе обработки, 2-отсчет до следующего сбора, 3 - не готов в приемнике

        #region Properties
        public string BeforeAlmanac
        {
            get { return beforeAlmanac; }
            private set
            {
                beforeAlmanac = value;
                PropertyChanged(this, new PropertyChangedEventArgs("BeforeAlmanac"));
            }
        }

        public string BeforeEphemeris
        {
            get { return beforeEphemeris; }
            private set
            {
                beforeEphemeris = value;
                PropertyChanged(this, new PropertyChangedEventArgs("BeforeEphemeris"));
            }
        }

        public byte EphState
        {
            get { return ephState; }
            private set
            {
                ephState = value;
                PropertyChanged(this, new PropertyChangedEventArgs("EphState"));
            }
        }

        public byte AlmState
        {
            get { return almState; }
            private set
            {
                almState = value;
                PropertyChanged(this, new PropertyChangedEventArgs("AlmState"));
            }
        }


        public DateTime CurrentDateTime
        {
            get { return _now; }
            private set
            {
                _now = value;
                PropertyChanged(this, new PropertyChangedEventArgs("CurrentDateTime"));
            }
        }

        public int SatelliteCount
        {
            get { return satelliteCount; }
            private set
            {
                satelliteCount = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SatelliteCount"));
            }
        }

        public double Latitude
        {
            get { return latitude; }
            private set
            {
                latitude = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Latitude"));
            }
        }

        public double Longitude
        {
            get { return longitude; }
            private set
            {
                longitude = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Longitude"));
            }
        }

        public double Altitude
        {
            get { return altitude; }
            private set
            {
                altitude = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Altitude"));
            }
        }

        void timer_Tick(object sender, EventArgs e)
        {
            CurrentDateTime = DateTime.Now;
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
