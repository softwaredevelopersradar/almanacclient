﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using System.Threading;
using System.Globalization;
using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.Axes;
using Arction.Wpf.SemibindableCharting.EventMarkers;
using Arction.Wpf.SemibindableCharting.Views.ViewPolar;
using ClientServerLib;

namespace Radar
{
    public partial class RadarControl : UserControl
    {
        private LightningChartUltimate _chart;
        
        PolarSeriesPoint[] dataPoints;


        private double mouseAmplitude = 0.0;
        private double mouseAngle = 0.0;
        public event EventHandler<float> SelectPointEvent;
        protected virtual void OnSelectPoint(EventHandler<float> some_ev, float value) => some_ev?.Invoke(this, value);

        PolarEventMarkerCollection myMarkers;

        DispatcherTimer _timerAlm;
        DispatcherTimer _timerEph;
        TimeSpan _timeAlm;
        TimeSpan _timeEph;

        public RadarControl()
        {
            DataContext = this;
            string deploymentKey = "lgCAABW2ij+vBNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA2LTE1I1JldmlzaW9uPTACgD+BCRGnn7c6dwaDiJovCk5g5nFwvJ+G60VSdCrAJ+jphM8J45NmxWE1ZpK41lW1wuI4Hz3bPIpT7aP9zZdtXrb4379WlHowJblnk8jEGJQcnWUlcFnJSl6osPYvkxfq/B0dVcthh7ezOUzf1uXfOcEJ377/4rwUTR0VbNTCK601EN6/ciGJmHars325FPaj3wXDAUIehxEfwiN7aa7HcXH6RqwOF6WcD8voXTdQEsraNaTYbIqSMErzg6HFsaY5cW4IkG6TJ3iBFzXCVfvPRZDxVYMuM+Q5vztCEz5k+Luaxs+S+OQD3ELg8+y7a/Dv0OhSQkqMDrR/o7mjauDnZVt5VRwtvDYm6kDNOsNL38Ry/tAsPPY26Ff3PDl1ItpFWZCzNS/xfDEjpmcnJOW7hmZi6X17LM66whLUTiCWjj81lpDi+VhBSMI3a2I7jmiFONUKhtD91yrOyHrCWObCdWq+F5H4gjsoP0ffEKcx658a3ZF8VhtL8d9+B0YtxFPNBQs=";
            Arction.Wpf.SemibindableCharting.LightningChartUltimate.SetDeploymentKey(deploymentKey);
            InitializeComponent();

            _chart = (Content as Grid).Children[0] as LightningChartUltimate;
            _chart.ViewPolar.LegendBox.ShowCheckboxes = false;
            _chart.ViewPolar.Axes[0].Reversed = true;
            _chart.ViewPolar.Axes[0].MinAmplitude = 0;
            _chart.ViewPolar.Axes[0].MaxAmplitude = 90;
            _chart.ViewPolar.Axes[0].AmplitudeLabelsVisible = false;
            _chart.ViewPolar.Axes[0].AutoFormatLabels = false;
            _chart.ViewPolar.Axes[0].SupplyCustomAngleString += AngleAsString;

            _now = DateTime.Now;

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(500);
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();



            myMarkers = new PolarEventMarkerCollection();
            //UpdatePoints(test);
            //UpdateGNSSCoordinates(35.632133, 125.785452, 255);
            SelectPointEvent += ShowInTable;
            SatTableControl.SelectPointEvent += ShowPointOnRadar;
        }

        private void ShowPointOnRadar(object sender, float[] angleAmplitude)
        {
            SelectPointFromTable(angleAmplitude[0], angleAmplitude[1]);
        }

        private void ShowInTable(object sender, float satNumber)
        {
            SatTableControl.ShowSelectedSat(satNumber);
        }

        float[] test = new float[15] { 165.5f, 35.5f, 1, 2, 0, 320, 80, 32, 1, 0, 60, 50, 15, 0, 90 };  //для теста (координаты спутников)

        public enum Timers
        {
            Almanac,
            Ephemeris,
            Stop
        }

        public void StartTimerAlm(Timers type, double seconds)
        {
            
            switch (type)
            {
                case Timers.Almanac:
                    _timeAlm = TimeSpan.FromSeconds(seconds);
                    if (_timerAlm != null)
                    { _timerAlm.Stop(); _timerAlm = null; }

                    _timerAlm = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
                    {
                        BeforeAlmanac = _timeAlm.ToString(@"mm\:ss");
                        if (_timeAlm == TimeSpan.Zero) _timerAlm.Stop();
                        _timeAlm = _timeAlm.Add(TimeSpan.FromSeconds(-1));
                    }, Application.Current.Dispatcher);

                    _timerAlm.Start();
                    break;
                case Timers.Ephemeris:
                    _timeEph = TimeSpan.FromSeconds(seconds);
                    if (_timerEph != null)
                    { _timerEph.Stop(); _timerEph = null; }

                    _timerEph = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
                    {
                        BeforeEphemeris = _timeEph.ToString(@"mm\:ss");
                        if (_timeEph == TimeSpan.Zero) _timerEph.Stop();
                        _timeEph = _timeEph.Add(TimeSpan.FromSeconds(-1));
                    }, Application.Current.Dispatcher);

                    _timerEph.Start();
                    break;
                case Timers.Stop:
                    _timerAlm?.Stop(); 
                    _timerEph?.Stop();
                    break;
                default:
                    break;
                   
            }
            
        }

        // Замена градусов на стороны света (на круге)
        private void AngleAsString(object sender, SupplyCustomAngleStringEventArgs args)
        {
            int degrees = (int)Math.Round(180f * args.Angle / Math.PI);
            if (degrees == 0)
                args.AngleAsString = "N";

            else if (degrees == 180)
                args.AngleAsString = "S";

            else if (degrees == 90)
                args.AngleAsString = "E";

            else if (degrees == 270)
                args.AngleAsString = "W";
        }


        #region User commands

        /// <summary>
        /// Update satellites coordinates info
        /// </summary>
        public void UpdateSatsData(List<SatInfo> sats)
        {
            if (sats.Count > 0)
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    UpdatePoints(sats);
                });
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    SatTableControl.UpdateSatInfo(sats);
                });
            }
        }


        private void UpdatePoints(List<SatInfo> sats)
        {
            try
            {
                int satsCount = sats.Count;
                int trackedSatsCount = 0;
                dataPoints = new PolarSeriesPoint[satsCount];
                for (int i = 0; i < satsCount; i++)
                {
                    dataPoints[i].Angle = sats[i].Azimuth;
                    dataPoints[i].Amplitude = 90 - sats[i].Elevation;  //отнимаю от 90, чтобы в центре круга была амплитуда 90
                    dataPoints[i].Value = sats[i].PRN;
                    dataPoints[i].Tag = sats[i].AcquisitionFlag;

                    if (sats[i].AcquisitionFlag == 2)
                    { trackedSatsCount++; }
                }
                UpdateSatCount(trackedSatsCount);

                myMarkers = CreateMarkers(dataPoints);

                _chart.ViewPolar.Markers = myMarkers; //возможно сделаю привязку
                _chart.ViewPolar.PointLineSeries[0].Points = dataPoints;
            }
            catch (Exception e)
            { }
        }


        Color color;
        private PolarEventMarkerCollection CreateMarkers(PolarSeriesPoint[] dataPoints)
        {
            var Markers = new PolarEventMarkerCollection();
            //Markers.Clear();
            foreach (PolarSeriesPoint p in dataPoints)
            {
                PolarEventMarker marker = new PolarEventMarker();
                marker.Amplitude = p.Amplitude;
                marker.AngleValue = p.Angle;
                marker.Label.Text = p.Value.ToString();
                _chart.ViewPolar.PointLineSeries[0].ValueRangePalette.GetColorByValue(Convert.ToDouble(p.Tag), out color);
                marker.Symbol.Color1 = color;
                marker.Symbol.Color2 = ChartTools.CalcGradient(color, Colors.Black, 0);
                marker.Label.Color = Colors.Black;
                marker.Label.Font.Size = 14;
                marker.Label.Distance = 0;
                marker.Label.VerticalAlign = AlignmentVertical.Center;
                marker.Label.HorizontalAlign = AlignmentHorizontal.Center;
                marker.MoveByMouse = false;
                marker.Symbol.GradientFill = GradientFillPoint.Solid;
                marker.Symbol.Shape = Arction.Wpf.SemibindableCharting.Shape.Circle;
                marker.Symbol.Width = 25;
                marker.Symbol.Height = 25;

                Markers.Add(marker);
            }
            return Markers;
        }

        /// <summary>
        /// Update Ephemeris status
        /// </summary>
        public void UpdateEphemerisState(byte status)
        {
            try
            {
                EphState = status;
            }
            catch { }
        }

        /// <summary>
        /// Update Almanac status
        /// </summary>
        public void UpdateAlmanacState(byte status)
        {
            try
            {
                AlmState = status;
            }
            catch { }
        }

        /// <summary>
        /// Update Receiver coordinates
        /// </summary>
        public void UpdateGNSSCoordinates(double latitude, double longitude, double altitude)
        {
            try
            {
                Latitude = latitude;
                Longitude = longitude;
                Altitude = altitude;
            }
            catch { }
        }

        /// <summary>
        /// Update amount of tracked satellites 
        /// </summary>
        public void UpdateSatCount(int countTrackedSats)
        {
            try
            {
                SatelliteCount = countTrackedSats;
            }
            catch { }
        }


        /// <summary>
        ///Clear
        /// </summary>
        public void ClearAll()
        {
            try
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    UpdatePoints(new List<SatInfo>());
                });
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    SatTableControl.UpdateSatInfo(new List<SatInfo>());
                });
                _timerAlm?.Stop();
                _timerEph?.Stop();
            }
            catch { }
        }
        #endregion

        //нажатие кнопки мыши
        private void chart_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Point mousePos = e.GetPosition(_chart);

            mouseAmplitude = GetMouseAmplitude(mousePos);
            mouseAngle = GetMouseAngle(mousePos);

            if (mouseAmplitude < _chart.ViewPolar.Axes[0].MaxAmplitude)
            {
                _chart.BeginUpdate();
                SelectPoint(mouseAngle, mouseAmplitude);
                _chart.EndUpdate();
            }
        }


        /// <summary>
        /// Get polar angle at give mouse screen coordinate.
        /// </summary>
        /// <param name="mouseCoord">Mouse screen coordinate.</param>
        /// <returns>Angle in degrees</returns>
        private double GetMouseAngle(Point mouseCoord)
        {
            double angle;
            double amplitude;
            _chart.ViewPolar.Axes[0].CoordToValue((int)Math.Round(mouseCoord.X), (int)Math.Round(mouseCoord.Y), out angle, out amplitude, true);

            return angle;
        }

        /// <summary>
        /// Get polar amplitude at give mouse screen coordinate.
        /// </summary>
        /// <param name="mouseCoord">Mouse screen coordinate.</param>
        /// <returns>Amplitude</returns>
        private double GetMouseAmplitude(Point mouseCoord)
        {
            double angle;
            double amplitude;
            _chart.ViewPolar.Axes[0].CoordToValue((int)Math.Round(mouseCoord.X), (int)Math.Round(mouseCoord.Y), out angle, out amplitude, true);

            return amplitude;
        }

        /// <summary>
        /// Select point 
        /// </summary>
		private void SelectPoint(double pointAngle, double pointAmplitude)
        {
            if (pointAngle < 0)
                pointAngle += 360;

            if (dataPoints == null)
                return;

            int pointCount = dataPoints.Length;
            
            List<PolarSeriesPoint> listSelectedPoints = new List<PolarSeriesPoint>();

            for (int i = 0; i < pointCount; i++)
            {
                if ((dataPoints[i].Angle >= pointAngle - 5 && dataPoints[i].Angle <= pointAngle + 5) &&
                    (dataPoints[i].Amplitude >= pointAmplitude - 5 && dataPoints[i].Amplitude <= pointAmplitude + 5))
                {
                    listSelectedPoints.Add(dataPoints[i]);
                }
            }
            if (myMarkers.Count > dataPoints.Length)
            {
                myMarkers.Remove(myMarkers.Last());
            }
            if (listSelectedPoints.Count != 0)
            {
                OnSelectPoint(SelectPointEvent, listSelectedPoints[0].Value);
                CreateSelectedMarker(listSelectedPoints[0]);
            }
        }

        private void CreateSelectedMarker(PolarSeriesPoint selectedPoint)
        {
            CultureInfo ci = new CultureInfo("en-us");
            PolarEventMarker selectedMarker = new PolarEventMarker();
            selectedMarker.Amplitude = selectedPoint.Amplitude;
            selectedMarker.AngleValue = selectedPoint.Angle;
            selectedMarker.Label.Text = selectedPoint.Angle.ToString("0.0", ci) + "°, " + (90 - selectedPoint.Amplitude).ToString("0.0", ci) + "°";
            selectedMarker.Label.Color = Colors.Cyan;
            selectedMarker.Label.Distance = 0;
            selectedMarker.MoveByMouse = false;
            selectedMarker.Symbol.BorderWidth = 2.0f;
            selectedMarker.Symbol.BorderColor = Colors.Cyan;
            selectedMarker.Symbol.GradientFill = GradientFillPoint.Solid;
            selectedMarker.Symbol.Color1 = Colors.Transparent;
            selectedMarker.Symbol.Shape = Arction.Wpf.SemibindableCharting.Shape.Circle;
            selectedMarker.Symbol.Width = 25;
            selectedMarker.Symbol.Height = 25;

            myMarkers.Add(selectedMarker);

            _chart.ViewPolar.Markers = myMarkers;
        }

        /// <summary>
        /// Select points from table
        /// </summary>
		public void SelectPointFromTable(double angle, double amplitude)
        {
            if (angle < 0)
                angle += 360;
            if (myMarkers.Count > dataPoints.Length)
            {
                myMarkers.Remove(myMarkers.Last());
            }
            var point = new PolarSeriesPoint() {Angle = angle, Amplitude = 90-amplitude };
            CreateSelectedMarker(point);
        }
    }

    public partial class RadarControl 
    {
        public  void Dispose()
        {
            if (_chart != null)
            {
                (Content as Grid).Children.Remove(_chart);

                _chart.Dispose();
                _chart = null;
            }

            //base.DisposedOf();
        }
    }
}
