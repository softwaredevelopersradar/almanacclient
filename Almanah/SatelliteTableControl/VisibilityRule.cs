﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SatelliteTableControl
{
    [ValueConversion(typeof(float), typeof(string))]
    public class VisibilityRule : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            float input = System.Convert.ToSingle(value);
            string output = System.Convert.ToString(value);
            if (input == -1)
            {
                output = "";
            }
            return output;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
