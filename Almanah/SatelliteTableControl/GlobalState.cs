﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using ClientServerLib;

namespace SatelliteTableControl
{
    class GlobalState : INotifyPropertyChanged
    {
        public ObservableCollection<SatInfo> FullParams { get; set; }

        public GlobalState()
        {
            FullParams = new ObservableCollection<SatInfo> { };
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
