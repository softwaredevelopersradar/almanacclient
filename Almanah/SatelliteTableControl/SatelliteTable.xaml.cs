﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Data;
using ClientServerLib;
using System.Collections.Generic;

namespace SatelliteTableControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class SatelliteTable : UserControl
    {
        public event EventHandler<float[]> SelectPointEvent;
        protected virtual void OnSelectPoint(EventHandler<float[]> some_ev, float[] azElev) => some_ev?.Invoke(this, azElev);

        bool deleteSelection = false;
        bool selectFromRadar = true;

        public SatelliteTable()
        {
            InitializeComponent();
            paramSatellite.DataContext = new GlobalState();
            paramSatellite.ItemsSource = (paramSatellite.DataContext as GlobalState).FullParams;
            AddEmptyRows();
            paramSatellite.SelectionChanged += ParamSatellite_SelectedCellsChanged;
            //Testc();
        }

        /// <summary>
        /// Если в таблице выбрали строку
        /// </summary>
        private void ParamSatellite_SelectedCellsChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!deleteSelection && !selectFromRadar)
            {
                DataGrid dataGrid = sender as DataGrid;
                var forSend = new float[2];
                forSend[0] = (dataGrid.SelectedCells[0].Item as SatInfo).Azimuth;
                forSend[1] = (dataGrid.SelectedCells[0].Item as SatInfo).Elevation;
                if (e.AddedItems.Count != 0 && forSend[0] != -1)
                {
                    OnSelectPoint(SelectPointEvent, forSend);
                }
            }
        }

        
        /// <summary>
        /// Подсветить строку
        /// </summary>
        public void ShowSelectedSat(float satNum)
        {
            try
            {
                selectFromRadar = true;
                var pos = ((GlobalState)paramSatellite.DataContext).FullParams.IndexOf(((GlobalState)paramSatellite.DataContext).FullParams.Where(item => item.PRN == satNum).FirstOrDefault());
                if (pos == -1)
                { paramSatellite.SelectedItem = null; }
                else
                { paramSatellite.SelectedItem = paramSatellite.Items[pos]; }
                selectFromRadar = false;
            }
            catch { }
        }
        

        /// <summary>
        /// Обновить значение координат
        /// </summary>
        public void UpdateSatInfo(List<SatInfo> targets)
        {
            try
            {
                deleteSelection = true;
                ((GlobalState)paramSatellite.DataContext).FullParams.Clear();
                for (int i = 0; i < targets.Count; i++)
                {
                    ((GlobalState)paramSatellite.DataContext).FullParams.Add(new SatInfo(targets[i].PRN, targets[i].SNR, targets[i].Azimuth, targets[i].Elevation, targets[i].AcquisitionFlag) );
                }
                paramSatellite.ItemsSource = ((GlobalState)paramSatellite.DataContext).FullParams;
                AddEmptyRows();
                deleteSelection = false;
            }
            catch { }
        }


        private void DataGridparamSatellite_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }


        /// <summary>
        /// Добавление пустых строк в таблицу
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                paramSatellite.Items.Refresh();
                int сountRowsAll = paramSatellite.Items.Count; // количество имеющихся строк в таблице
                double hs = 23; // высота строки
                double ah = paramSatellite.ActualHeight; // визуализированная высота dataGrid
                double chh = paramSatellite.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                for (int i = 0; i < count; i++)
                {
                    int index = ((GlobalState)paramSatellite.DataContext).FullParams.ToList().FindIndex(x => x.PRN < 0);
                    if (index != -1)
                    {
                        ((GlobalState)paramSatellite.DataContext).FullParams.RemoveAt(index);
                    }
                }
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    ((GlobalState)paramSatellite.DataContext).FullParams.Add(new SatInfo());
                }
            }
            catch { }
        }
    }
}
